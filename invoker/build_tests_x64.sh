echo "Building tests for nucleus project"
cd /home/git/nucleus/tests/unit
BUILD_DIR=build/tests/unit/x64
cmake -H. -B"$BUILD_DIR" -Dtest_build=ON -Drpi_machine=OFF
cmake --build $BUILD_DIR -- -j4
