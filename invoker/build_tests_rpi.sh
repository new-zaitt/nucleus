#!/bin/bash

## Script to build raspbian compiled application.
cd /home/git/nucleus/
TC_PATH=/home/raspi/tools
BUILD_DIR=build/tests/unit/rpi/
SYS_ROOT_PATH=/home/raspi/sysroot
RPI_TC_CMAKE=invoker/toolchain
mkdir -p $BUILD_DIR
cmake -H. -B"$BUILD_DIR" -DCMAKE_TOOLCHAIN_FILE="$RPI_TC_CMAKE"/raspbian-tc.cmake \
      -DCMAKE_THREAD_LIBS_INIT="$SYS_ROOT_PATH"/usr/lib -Dtest_build=ON
cmake --build $BUILD_DIR --
