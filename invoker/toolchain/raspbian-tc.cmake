# Define our host system
SET(CMAKE_SYSTEM_NAME Linux)
SET(CMAKE_SYSTEM_VERSION 1)
# Define the cross compiler locations
SET(TOOLCHAIN_PREFIX /home/raspi/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin/arm-linux-gnueabihf-)
SET(CMAKE_C_COMPILER ${TOOLCHAIN_PREFIX}gcc)
SET(CMAKE_CXX_COMPILER ${TOOLCHAIN_PREFIX}g++)

# Define the sysroot path for the RaspberryPi distribution in our tools folder
SET(CMAKE_SYSROOT /home/raspi/sysroot/)
set(CMAKE_FIND_ROOT_PATH ${CMAKE_SYSROOT} /home/raspi/sysroot/lib/arm-linux-gnueabihf/)
# Use our definitions for compiler tools
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# Search for libraries and headers in the target directories only
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
SET(Boost_USE_STATIC_LIBS ON)
SET(Boost_USE_STATIC_RUNTIME ON)
SET(Boost_INCLUDE_DIR ${CMAKE_SYSROOT}/usr/local/boost/include)
SET(Boost_LIBRARY_DIR ${CMAKE_SYSROOT}/usr/local/boost/lib)
add_definitions(-Wall -std=c++11 -D_GNU_SOURCE)
