#ifndef DEFINES_H
#define DEFINES_H

#define PERIODIC "/home/nucleus/periodic/"
#define SPORADIC "/home/nucleus/sporadic/"
#define CONFIGURATION_FILE SPORADIC"configuration_file"
#define LOG_FILE PERIODIC"nucleus.log"
#define SNAPSHOT_FILE PERIODIC"nucleus_snapshot"

#endif
