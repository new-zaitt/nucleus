#ifndef PUSHERCLIENT_H
#define PUSHERCLIENT_H
#include <QObject>

#include <QTimer>

#include <string>
#include <utility>
#include <functional>
#include <memory>
#include <map>

#include "src/FileManager.h"
#include "pusher-websocket-cpp/pushcpp.h"
#include "spdlog/spdlog.h"

namespace nucleus {
namespace iot_client {
class PusherClient : public QObject {
 Q_OBJECT

 public:
     PusherClient(IoTConfig pusher_config,
                  std::shared_ptr<spdlog::logger> logger);
     virtual ~PusherClient();
     void Connect();
     void SubscribeToEvent(const std::string &event_type,
                           std::function<void(const std::string &data)>
                                callback);
     void Disconnect();
     void Execute();
     void SendResponse(const std::string &event_type, const std::string &data);
     bool IsConnected() const;
 private:
     static int kActivityTimeout;
     static int kPongTimeout;
     void ConnectionHandler(const pushcpp::ConnectionEvent ev, const std::string& message);
     void ErrorHandler(const int code, const std::string& message);
     void EventHandler(const std::string &channel,
                       const std::string &event,
                       const std::string &data);
     void PingHandler(const pushcpp::PingEvent event);
     void PingPusher() const;
     std::string key_;
     std::string channel_;
     std::string cluster_;
     std::shared_ptr<pushcpp> pusher_client_;
     std::shared_ptr<spdlog::logger> logger_;
     QTimer *ping_timeout_;
     QTimer *pong_timeout_;
     std::map<std::string, std::function<void(const std::string&)>>
            event_callbacks_;
     int reconnect_counter_;
     bool connected_;
     bool connect_request_;

 signals:
     void ConnectedSignal();
};

}
}
#endif
