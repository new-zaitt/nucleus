
#include "PusherClient.h"
#include <QObject>

#include <cstdlib>
#include <functional>
#include <map>
#include <string>
#include <utility>

#include "src/FileManager.h"
#include "spdlog/spdlog.h"

namespace nucleus {
namespace iot_client {

int PusherClient::kActivityTimeout = 10000;
int PusherClient::kPongTimeout = 5000;
PusherClient::PusherClient(IoTConfig pusher_config,
                           std::shared_ptr<spdlog::logger> logger)
    : key_(pusher_config.key),
      channel_(pusher_config.channel),
      cluster_(pusher_config.cluster),
      logger_(logger),
      reconnect_counter_(0),
      connected_(false),
      connect_request_(false) {

    pusher_client_ = std::make_shared<pushcpp>(key_,
            std::bind(&PusherClient::ConnectionHandler, this, std::placeholders::_1,
                      std::placeholders::_2),
            std::bind(&PusherClient::ErrorHandler, this, std::placeholders::_1,
                      std::placeholders::_2),
            std::bind(&PusherClient::PingHandler, this, std::placeholders::_1),
            cluster_);
    logger_->info("PusherClient: Initializing pusher client.");
    pusher_client_->subscribe(channel_,
            std::bind(&PusherClient::EventHandler, this,
                      std::placeholders::_1,
                      std::placeholders::_2,
                      std::placeholders::_3));
    ping_timeout_ = new QTimer(this);
    pong_timeout_ = new QTimer(this);
    connect(ping_timeout_, &QTimer::timeout, this, &PusherClient::PingPusher);
    connect(pong_timeout_, &QTimer::timeout, this, &PusherClient::Disconnect);
}

PusherClient::~PusherClient() {
    ping_timeout_->deleteLater();
    pong_timeout_->deleteLater();
}

void PusherClient::Connect() {
    logger_->info("PusherClient: Connecting to pusher server.");
    pusher_client_->connect();
    connect_request_ = true;
}

void PusherClient::Disconnect() {
    logger_->info("PusherClient: Disconnecting pusher client due to timeout. Trying to reconnect in"
                  " 10 seconds.");
    pusher_client_->disconnect();
}

void PusherClient::Execute() {
    if (!connect_request_ && !connected_ ) {
        if (reconnect_counter_ == 100) {
            if (!pusher_client_->connected())
                Connect();
            reconnect_counter_=0;
        } else {
            reconnect_counter_++;
        }
    }
    pusher_client_->Execute();
}

void PusherClient::ConnectionHandler(const pushcpp::ConnectionEvent event,
                                     const std::string& message) {
    if (static_cast<int>(event) == 0) {
        logger_->info("PusherClient: Pusher client connected successfully.");
        connected_ = true;
        ConnectedSignal();
        logger_->info("PusherClient: Connect message : {} ", message);
        if (ping_timeout_->isActive())
            ping_timeout_->stop();
        ping_timeout_->start(kActivityTimeout);
    } else {
        logger_->info("PusherClient: Disconnected from remote server");
        ping_timeout_->stop();
        pong_timeout_->stop();
        connected_ = false;
        connect_request_ = false;
    }

}

void PusherClient::ErrorHandler(const int code, const std::string &message) {
    logger_->error("PusherClient: There was some error with pusher requisition \
                    Code: {0:d}. Error Message: {}", code, message);
}

void PusherClient::EventHandler(const std::string &channel,
                                const std::string &event,
                                const std::string &data) {
    logger_->info("PusherClient: Received event {} from channel {}.", event,
                  channel);
    logger_->info("PusherClient: Data received: {}.", data);
    try {
        event_callbacks_.at(event)(data);
    } catch (std::out_of_range ex) {
        logger_->error("PusherClient: Received an event with no registered "
                       "callback. Event type: {}", event);
    }

}

void PusherClient::SendResponse(const std::string &event_type,
                               const std::string &data) {
    logger_->info("PusherClient: Trigger event type: {}. Sending data: {}",
                  event_type, data);
    pusher_client_->trigger(channel_, event_type, data);
}

void PusherClient::SubscribeToEvent(const std::string &event_type,
        std::function<void(const std::string &data)> callback) {
    logger_->info("PusherClient: Register calback to event: {}", event_type);
    event_callbacks_[event_type] = callback;
}

bool PusherClient::IsConnected() const {
    return connected_;
}

void PusherClient::PingHandler(const pushcpp::PingEvent event) {
    //logger_->info("PusherClient: Received a pong event");
    if (pong_timeout_->isActive())
        pong_timeout_->stop();
}

void PusherClient::PingPusher() const {
    //logger_->info("PusherClient: Send ping to channel: {}", channel_);
    pusher_client_->send(channel_, "pusher:ping", "");
    pong_timeout_->start(kPongTimeout);
}

}
}
