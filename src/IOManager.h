#ifndef IOMANAGER_H
#define IOMANAGER_H

#include <QJsonObject>

#include <string>
#include <map>
#include <memory>
#include <mutex>

#include "file_system/JSonFile.h"
#include "FileManager.h"
#include "softPwm.h"
#include "spdlog/spdlog.h"
#include "wiringPi.h"


class IOManager {

 public:
     explicit IOManager(std::shared_ptr<spdlog::logger> logger,
            IOPins pins, VarStatus snapshot_vars);
     virtual ~IOManager();
     void Write(OutputPins pin, bool value) const;
     void WritePwm(OutputPins pin, int32_t value);
     bool Read(InputPins pin) const;
     bool Read(OutputPins pin) const;

 private:
     void SetUp();
     void SetPinsInitValues(VarStatus snapshot_vars);
     std::shared_ptr<spdlog::logger> logger_;
     IOPins pins_;
     std::mutex mutex_;
};

#endif
