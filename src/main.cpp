/*
 * main.cpp
 *
 *  Created on: 5 de jun de 2018
 *      Author: andregms
 */
#include <QCoreApplication>
#include <QJsonObject>
#include <QJsonDocument>

#include <iostream>
#include <memory>
#include <string>
#include <cstdlib>

#include "defines.h"
#include "FileManager.h"
#include "IOManager.h"
#include "iot_client/PusherClient.h"
#include "monitoring/MonitoringFacade.h"
#include "MainEngine.h"
#include "spdlog/spdlog.h"
#include "monitoring/Debouncer.h"
#include "monitoring/SnapshotManager.h"


using nucleus::iot_client::PusherClient;
using nucleus::monitoring::MonitoringFacade;
using nucleus::monitoring::Debouncer;
using nucleus::monitoring::SnapshotManager;

void CreatePathDirs();

int main(int argc, char *argv[]) {
		QCoreApplication main_app(argc, argv);
    CreatePathDirs();
    std::shared_ptr<spdlog::logger> logger = spdlog::basic_logger_mt("nucleus",
            LOG_FILE, false);
    logger->flush_on(spdlog::level::info);
    logger->info("Starting nucleus application.");
		FileManager file_manager = FileManager(logger);
    std::shared_ptr<IOManager> io_manager = std::make_shared<IOManager>(logger,
						file_manager.GetIOPinsStruct(), file_manager.GetSnapshotStruct());
    std::shared_ptr<PusherClient> pusher_client =
        	std::make_shared<PusherClient>(file_manager.GetIoTConfig(), logger);
    logger->info("Running pusher client");
		std::shared_ptr<MonitoringFacade> facade_ =
				std::make_shared<MonitoringFacade>(logger,io_manager,pusher_client,
							file_manager.GetSnapshotStruct());
    std::shared_ptr<Debouncer> exit_button = std::make_shared<Debouncer>(logger,
            io_manager);

    std::shared_ptr<MainEngine> main_engine =
            std::make_shared<MainEngine>(pusher_client, exit_button,
																				 facade_->GetSnapshotManager());

    pusher_client->Connect();
    return main_app.exec();
}

void CreatePathDirs() {
    const int ret = system("mkdir -p /home/nucleus");
    if (ret != -1) {
        std::cout << "Creating dir paths" << std::endl;
        system(" mkdir -p " PERIODIC);
        system(" mkdir -p " SPORADIC);
    } else {
        std::cout << "Could not create dirs. " << std::endl;
    }
}
