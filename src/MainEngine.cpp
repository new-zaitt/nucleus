#include "MainEngine.h"
#include <QObject>
#include <QTimer>

using nucleus::iot_client::PusherClient;
using nucleus::monitoring::Debouncer;
using nucleus::monitoring::SnapshotManager;
MainEngine::MainEngine(std::shared_ptr<PusherClient> pusher_client,
        std::shared_ptr<nucleus::monitoring::Debouncer> exit_button,
      std::shared_ptr<SnapshotManager> snapshot_manager)
        : pusher_client_(pusher_client),
          exit_button_(exit_button),
          snapshot_manager_(snapshot_manager){
    run_timer_ = new QTimer(this);
    connect(run_timer_, &QTimer::timeout, this,
            &MainEngine::Run);
    run_timer_->start(100);
}

MainEngine::~MainEngine() { }

void MainEngine::Run() {
    pusher_client_->Execute();
    snapshot_manager_->Execute();
    if (!pusher_client_->IsConnected()){
        exit_button_->Execute();
    }

}
