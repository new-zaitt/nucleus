#include "JSonFile.h"

namespace nucleus {
namespace file_system {
JSonFile::JSonFile(QString path, std::shared_ptr<spdlog::logger> logger)
        : logger_(logger),
          read_path_(path){

    logger_->info("JSonFile: Starting JSonFile.");
    ReadFileJson();
}

JSonFile::~JSonFile(){

}

void JSonFile::ReadFileJson(){

    if(!(QFileInfo::exists(read_path_) && QFileInfo(read_path_).isFile())){
        logger_->error("JSonFile: JSon file not located in {}.",
                read_path_.toStdString());
        return;
    } else{
        QString file_string;
        QFile file;
        file.setFileName(read_path_);
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        file_string = file.readAll();
        file.close();
        QJsonDocument json_document = QJsonDocument::fromJson(file_string.toUtf8());
        json_object_ = json_document.object();
        logger_->info("JSonFile: JSon file successfully read.");
    }
}

void JSonFile::WriteFileJson(QString file_name){
    QJsonDocument json_document(json_object_);
    QFile jsonFile(file_name);
    jsonFile.open(QFile::WriteOnly);
    jsonFile.write(json_document.toJson());
    logger_->info("JSonFile: JSon file successfully written as {}.",
            file_name.toStdString());
}

QJsonObject JSonFile::GetObject(){
    return json_object_;
}

}
}
