#ifndef SNAPSHOTMANAGER_H
#define SNAPSHOTMANAGER_H

#include <QDate>
#include <QFile>
#include <QFileInfo>
#include <QIODevice>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>

#include "MonitoringManager.h"
#include "src/defines.h"
#include "spdlog/spdlog.h"

namespace nucleus {
namespace monitoring {

class SnapshotManager {
    public:
        SnapshotManager(std::shared_ptr<spdlog::logger> logger,
                        std::shared_ptr<MonitoringManager> monitoring_manager);
        virtual ~SnapshotManager();
        void UpdateJson();
        void Execute();
    private:
        int snapshot_exec_counter_;
        QString file_name_;
        QJsonObject json_object_;
        std::shared_ptr<spdlog::logger> logger_;
        std::shared_ptr<MonitoringManager> monitoring_manager_;
};

}
}

#endif
