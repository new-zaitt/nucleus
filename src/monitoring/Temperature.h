#ifndef TEMPERATURE_H
#define TEMPERATURE_H

#include "DS18B20.h"
#include "spdlog/spdlog.h"

namespace nucleus {
namespace monitoring {
class Temperature {
    public:
        Temperature(std::shared_ptr<spdlog::logger> logger);
        Temperature(const char* address, //'28XXXXXXXXXXX'
                std::shared_ptr<spdlog::logger> logger);
        virtual ~Temperature();
        double GetValue();
    private:
        std::shared_ptr<spdlog::logger> logger_;
        std::shared_ptr<DS18B20> w1Device_;
};

}
}

#endif
