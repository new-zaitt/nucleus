#ifndef MONITORINGFACADE_H
#define MONITORINGFACADE_H

#include <QObject>

#include <memory>

#include "MonitoringManager.h"
#include "SnapshotManager.h"
#include "spdlog/spdlog.h"
#include "src/IOManager.h"
#include "src/iot_client/PusherClient.h"
#include "src/dbus/RestClientApi.h"

namespace nucleus {
namespace monitoring {

class MonitoringFacade : public QObject {
 Q_OBJECT

 public:
     MonitoringFacade(std::shared_ptr<spdlog::logger> logger,
            std::shared_ptr<IOManager> io_manager,
            std::shared_ptr<nucleus::iot_client::PusherClient> pusher_client,
            VarStatus snapshot_vars);

    std::shared_ptr<SnapshotManager> GetSnapshotManager();

 private:
     std::shared_ptr<nucleus::dbus::RestClientApi> rest_client_;
     std::shared_ptr<MonitoringManager> monitoring_manager_;
     std::shared_ptr<SnapshotManager> snapshot_manager_;
};

}
}

#endif
