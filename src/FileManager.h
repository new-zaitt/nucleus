#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <memory>
#include <map>
#include <QDate>
#include <QDateTime>
#include <QFile>
#include <QFileInfo>
#include <QIODevice>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>
#include <QTime>
#include <string>


#include "file_system/JSonFile.h"
#include "spdlog/spdlog.h"

struct VarStatus {
    bool snapshot_valid = false;
    bool front_door_open;
    bool exit_door_open;
    bool ligth_front_rgb;
    bool ac_main_device;
    bool ac_aux_device;
    bool store_open;
};

struct IoTConfig {
    std::string key = "";
    std::string channel = "";
    std::string cluster = "us2";
};

enum class OutputPins {
    kFrontDoor,
    kExitDoor,
    kACDevice,
    kACDeviceAux,
    kLightCircuit,
    kLightFacadePower,
    kLightFacadeChannelR,
    kLightFacadeChannelG,
    kLightFacadeChannelB,
};

enum class InputPins {
    kFrontDoor,
    kExitDoor,
    kSmokeDetector,
    kTemperatureSensor,
    kExitDoorEmergency,
};

struct IOPins {
    std::map<OutputPins, int> output_pins_;
    std::map<InputPins, int> input_pins_;
};

class FileManager {
 public:
     FileManager(std::shared_ptr<spdlog::logger> logger);
     virtual ~FileManager();
     void ParseJson();
     IoTConfig GetIoTConfig();
     IOPins GetIOPinsStruct();
     VarStatus GetSnapshotStruct();
 private:
     void ParsePinsJson();
     void ParseJsonOutput(QJsonObject output_object);
     void ParseJsonInput(QJsonObject input_object);
     void ParseSnapshot();
     nucleus::file_system::JSonFile json_file_;
     nucleus::file_system::JSonFile snapshot_file_;
     IoTConfig pusher_config_;
     static char* kJsonKeyOutputPins;
     static char* kJsonKeyInputPins;
     IOPins pins_;
     VarStatus snapshot_vars_;
     std::shared_ptr<spdlog::logger> logger_;

};

#endif
